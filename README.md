#DarkAges

A re-imagining of the game Colony Survival.

PLEASE NOTE: This mod modifies a considerable amount of vanilla .JSON files. Only two blocks which cannot be added with JSONs alone are in the mod file. So presently the only way to install and use this mod is to overwrite your /gamedata directory with the one provided. Do not do this without backing up your existing folder first for easy reversion if you don't like the changes.

PLEASE NOTE: This mod includes within its files a slightly modified version of Scarabol's Construction mod. This is not my work, and all credit for its genious should go to its original author. My modifications were limited to making the build tool and blueprints accessible at game start, instead of requiring later materials first.

LASTLY: This mod is intended to be used with other mods, such as Pandaros' Settlers mod. As such, I renamed existing crafting stations and items in almost all cases rather than making new ones so that mods which modify these crafting stations will still work. In theory, any and all mods except those that modify the kiln should be compatible. The downside of this is due to limited changes to the Translation.json file many of the changes will not appear correctly in any language except en-US.

GOAL: The aim of this mod is to address as many things about the vanilla implimentation that do not make sense as possible, and also to make the various crafting stations more fluid and easier to use so you can spend more time building your colony and less time troubleshooting why you're not getting enough of the one thing you need. Lastly, the mod aims to make the early game more realistic of the early stone age, so that there is more progression in building materials and technology. A side effect of this should be to make the game more accessible for new players.

Currently Implimented:

No more building that majestic castle from day one. Your colony will start out living in cob huts and you will have to advance to the point of castle-building and beyond.
There is a purpose for berries now. Most people simply skip right over berries or use them very little -jumping right to wheat and bread production. This game makes flour and bread harder to atchieve therefor forcing the player to rely on berries early game.
Streamlined production. Ever get tired of your metalsmith complaining he has no bronze plates, when he's the one responsible for making them in the first place? No more. Now every station depends upon things made at an earlier station. Thus there is more a sense of progression and smoother running assembly lines.
More useful colonists. You can have your colonists build buildings for you from day one. No need to building everything yourself.
Next To Be Implimented: 5) Common sense research. Scientists should not be inventing things by making a whole bunch of different colored bags, and textiles should be more important for clothing manufacture than inventing rifles. This will be addressed.

Hope To Impliment Someday: 6) Thirst, water harvesting, and wells. 7) Clothing that your colonists actually wear, and penalties to leaving your colonists exposed to the elements without warm clothing. 8) Decorate blocks, and more late-game building materials.
